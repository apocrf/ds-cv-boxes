import os
from pathlib import Path

import click
import mmcv
from mmengine.fileio import dump
from mmengine.utils import track_iter_progress


@click.command()
@click.option(
    "--image-path",
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
)
@click.option(
    "--label-path",
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
)
@click.option(
    "--out-path",
    type=click.Path(),
)
def yolo_to_coco(
    image_path: Path,
    label_path: Path,
    out_path: Path,
) -> None:
    """
    Convert annotations from YOLO format to MS COCO.

    \b
    Example:
    poetry run yolo_to_coco \\
        --image-path data/boxes/train/images/ \\
        --label-path data/boxes/train/labels/ \\
        --out-path data/boxes/train.json
    """
    annotations = []
    images = []
    obj_count = 0
    image_list = os.listdir(image_path)
    for idx, filename in enumerate(track_iter_progress(image_list)):
        ip = os.path.join(image_path, filename)
        lp = os.path.join(label_path, os.path.splitext(filename)[0] + ".txt")
        if os.path.isfile(ip) and os.path.isfile(lp):
            img_height, img_width = mmcv.imread(ip).shape[:2]
            images.append(
                {
                    "id": idx,
                    "file_name": filename,
                    "height": img_height,
                    "width": img_width,
                }
            )

            with open(lp, encoding="utf-8") as f:
                for line in f:
                    _, center_x, center_y, w, h = line.strip().split()
                    width = float(w) * img_width
                    height = float(h) * img_height
                    x = float(center_x) * img_width - width / 2
                    y = float(center_y) * img_height - height / 2
                    data_anno = {
                        "image_id": idx,
                        "id": obj_count,
                        "category_id": 0,
                        "bbox": [x, y, width, height],
                        "area": width * height,
                        "segmentation": [
                            [
                                x,
                                y,
                                x + width,
                                y,
                                x + width,
                                y + height,
                                x,
                                y + height,
                                x,
                                y,
                            ]
                        ],
                        "iscrowd": 0,
                    }
                    annotations.append(data_anno)
                    obj_count += 1

    coco_format_json = {
        "images": images,
        "annotations": annotations,
        "categories": [{"id": 0, "name": "cardboard"}],
    }

    dump(coco_format_json, out_path)
