import json
from pathlib import Path

import click


@click.command()
@click.option(
    "-i",
    "--in-path",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-o",
    "--out-path",
    type=click.Path(),
)
def via_to_cvat(
    in_path: Path,
    out_path: Path,
) -> None:
    """
    Convert annotations from VIA COCO format to CVAT COCO

    \b
    Example:
    poetry run via_to_cvat \\
        --in-path data/raw/annotations.json \\
        --out-path data/raw/out.json
    """
    with open(in_path, encoding="utf-8") as f:
        annotations = json.load(f)

    image_ids = {}
    for i, image in enumerate(annotations["images"], start=1):
        image_id = image["id"]
        image_ids[image_id] = i
        image["id"] = i

    for annotation in annotations["annotations"]:
        annotation["image_id"] = image_ids[annotation["image_id"]]
        annotation["category_id"] += 1

    for category in annotations["categories"]:
        category["id"] += 1

    with open(out_path, "w", encoding="utf-8") as f:
        json.dump(annotations, f)
