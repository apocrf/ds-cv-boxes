# -*- coding: utf-8 -*-
from collections import defaultdict
import json
import logging
import os
from pathlib import Path
import random
import shutil
from typing import Any, Dict, List, Optional
import click


@click.command()
@click.option(
    "-i",
    "--input-filepath",
    default=Path("data/raw/"),
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
)
@click.option(
    "-o",
    "--output-filepath",
    default=Path("data/processed/"),
    show_default=True,
    type=click.Path(),
)
@click.option(
    "--annotations",
    default="annotations.json",
    show_default=True,
    type=click.STRING,
)
@click.option(
    "--test-images-list",
    default=None,
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    help="Path to file containing list of images for test subset",
)
@click.option(
    "--train-size",
    default=0.8,
    show_default=True,
    type=click.FloatRange(0, 1),
)
@click.option(
    "--random-state",
    default=0,
    show_default=True,
    type=click.IntRange(0, 2**32 - 1),
)
def train_test_split(
    input_filepath: Path,
    output_filepath: Path,
    annotations: str,
    test_images_list: Optional[Path],
    train_size: float,
    random_state: int,
) -> None:
    """
    Split dataset in MS COCO format into random train and test subsets.

    \b
    Example:
    poetry run train_test_split \\
        -i data/raw/ \\
        -o data/processed/ \\
        --annotations annotations.json \\
        --train-size 0.8 \\
        --random-state 0
    """

    def copy_data(
        images_data: List[Dict[str, Any]],
        destination_dir: str,
    ) -> None:
        """
        Copy images and corresponding annotations to the respective
        directories.
        """
        os.makedirs(destination_dir, exist_ok=True)
        annotations_subset = {
            "info": annotations_data["info"],
            "images": images_data,
            "annotations": [],
            "licenses": annotations_data["licenses"],
            "categories": annotations_data["categories"],
        }
        for image in images_data:
            shutil.copy(
                os.path.join(input_filepath, "images", image["file_name"]),
                os.path.join(destination_dir, image["file_name"]),
            )
            annotations_subset["annotations"].extend(
                annotations_by_image_id[image["id"]]
            )
        base_dir = os.path.dirname(destination_dir)
        filename = f"{os.path.basename(destination_dir)}.json"
        path = os.path.join(base_dir, filename)
        with open(path, "w", encoding="ASCII") as json_file:
            json.dump(annotations_subset, json_file)

    logger = logging.getLogger(__name__)
    logger.info("making train test split")

    # Clean output dir if it exists
    if os.path.exists(output_filepath):
        shutil.rmtree(output_filepath)
        os.makedirs(output_filepath, exist_ok=True)

    # Read annotations file
    with open(
        os.path.join(input_filepath, annotations), "r", encoding="ASCII"
    ) as f:
        annotations_data = json.load(f)

    # Create annotations cache by id
    annotations_by_image_id = defaultdict(list)
    for a in annotations_data["annotations"]:
        annotations_by_image_id[a["image_id"]].append(a)

    train_images = []
    test_images = []
    if test_images_list is None:
        # Shuffle the list of images
        random.seed(random_state)
        images_data = annotations_data["images"]
        random.shuffle(images_data)

        # Split the dataset
        train_size = int(train_size * len(images_data))
        train_images = images_data[:train_size]
        test_images = images_data[train_size:]
    else:
        # Load test images names
        test_images_set = set()
        with open(test_images_list, "r", encoding="ASCII") as f:
            for file_name in f.readlines():
                test_images_set.add(file_name.strip())

        # Split the dataset
        images_data = annotations_data["images"]
        for image_data in images_data:
            file_name = image_data["file_name"]
            if file_name in test_images_set:
                test_images.append(image_data)
            else:
                train_images.append(image_data)

    # Copy data to train and test
    copy_data(train_images, os.path.join(output_filepath, "train"))
    copy_data(test_images, os.path.join(output_filepath, "val"))
