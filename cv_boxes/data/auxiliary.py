# -*- coding: utf-8 -*-
import glob
import json
import os
from pathlib import Path
from typing import List, Literal


def load_annotations(path: Path):
    with open(path, encoding="utf-8") as f:
        return json.load(f)


def get_images_list(
    data_dir: Path,
    image_types: List[Literal["bmp", "jpg", "jpeg", "png"]],
) -> List[str]:
    """Returns list of images in data directory."""
    ret: List[str] = []
    for image_type in image_types:
        images = glob.glob(os.path.join(data_dir, f"*.{image_type}"))
        ret.extend(map(os.path.basename, images))

    return ret
