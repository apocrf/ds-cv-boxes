#!/usr/bin/env python3
from io import BytesIO
import os
import requests

from dotenv import load_dotenv
from telegram import (
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Update,
)
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

from cv_boxes.logging import get_logger

MAIN_MENU, IMAGE, TEXT = range(3)

logger = get_logger(3)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Starts the conversation and asks the user about their gender."""
    if update.message is None:
        return ConversationHandler.END

    reply_keyboard = [["Annotated image", "Text report"], ["Cancel"]]
    await update.message.reply_text(
        "Select an option please.\n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard,
            one_time_keyboard=True,
            input_field_placeholder="Select an option please",
        ),
    )
    return MAIN_MENU


async def image_report(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> int:
    """Draw bounding boxes on a photo and send it back."""
    if update.message is None:
        return ConversationHandler.END

    response = requests.get(
        url="http://localhost:8000/predict-image/",
        timeout=30,
    )
    if response.status_code == 200:
        out = BytesIO()
        out.write(response.content)
        out.seek(0)
        await update.message.reply_photo(photo=out)
    else:
        logger.error(
            "POST request failed. Status code: %d", response.status_code
        )

    logger.info("Sent report to %s", get_user_name(update))
    await update.message.reply_text("Send /start to get new report.")

    return ConversationHandler.END


async def text_report(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> int:
    """Count boxes on a photo and reply with a text report."""
    if update.message is None:
        return ConversationHandler.END

    response = requests.get(
        url="http://localhost:8000/predict-text/",
        timeout=30,
    )
    if response.status_code == 200:
        ret = [f"{label}: {count}" for label, count in response.json().items()]
        await update.message.reply_text("\n".join(ret))
    else:
        logger.error(
            "POST request failed. Status code: %d", response.status_code
        )

    logger.info("Sent report to %s", get_user_name(update))
    await update.message.reply_text("Send /start to get new report.")

    return ConversationHandler.END


def get_user_name(update: Update) -> str:
    if update.message is None:
        return ""

    user = update.message.from_user
    return user.first_name if user else ""


async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Cancels and ends the conversation."""
    if update.message is None:
        return ConversationHandler.END

    logger.info("User %s canceled the choice.", get_user_name(update))
    await update.message.reply_text(
        "Have a nice day!",
        reply_markup=ReplyKeyboardRemove(),
    )

    return ConversationHandler.END


def main() -> None:
    """Run the bot."""
    logger.info("Bot started.")
    load_dotenv()
    token = os.environ.get("TELEGRAM_TOKEN", "")
    application = Application.builder().token(token).build()

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            MAIN_MENU: [
                MessageHandler(
                    filters.Regex("^Annotated image$"), image_report
                ),
                MessageHandler(filters.Regex("^Text report$"), text_report),
                MessageHandler(filters.Regex("^Cancel$"), cancel),
            ],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )

    application.add_handler(conv_handler)

    application.run_polling(allowed_updates=Update.ALL_TYPES)


if __name__ == "__main__":
    main()
