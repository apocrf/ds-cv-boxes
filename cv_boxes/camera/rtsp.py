# -*- coding: utf-8 -*-
import os
from pathlib import Path
from typing import Tuple

import click
import cv2
from dotenv import load_dotenv
import numpy as np

from cv_boxes.logging import get_logger


load_dotenv()
logger = get_logger(3)

IP = os.environ.get("CAMERA_IP")
PORT = os.environ.get("CAMERA_PORT")
LOGIN = os.environ.get("CAMERA_LOGIN")
PASS = os.environ.get("CAMERA_PASS")
RTSP_STREAM_LINK = f"rtsp://{LOGIN}:{PASS}@{IP}:{PORT}/Streaming/Channels/101/"


@click.command()
@click.argument("output_path", type=click.Path(path_type=Path))
def capture_frame(output_path: Path) -> None:
    try:
        img = _capture_frame()
        cv2.imwrite(f"{output_path}", img)
    except cv2.error as e:
        logger.error("Error saving image: %s", e)


def _capture_frame() -> np.ndarray:
    img = np.empty((0, 0, 0), dtype=float)
    try:
        camera = cv2.VideoCapture(RTSP_STREAM_LINK)
        if camera.isOpened():
            _, img = camera.read()
    except cv2.error as e:
        logger.error("Error while capturing frame: %s", e)
    finally:
        camera.release()
        cv2.destroyAllWindows()

    return img


def get_image_size(device: cv2.VideoCapture) -> Tuple[int, int]:
    width = int(device.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(device.get(cv2.CAP_PROP_FRAME_HEIGHT))

    return width, height
