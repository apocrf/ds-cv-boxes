from collections import Counter
from typing import Dict, List, Tuple

import cv2
from matplotlib import patches
import matplotlib.pyplot as plt
import numpy as np


def draw_bboxes1(
    image: np.ndarray,
    bboxes: List[List[float]],
    labels: List[int],
    scores: List[float],
    classes: Tuple[str],  # ('Keenetic Start', ...)
    palette: List[Tuple[int, int, int]],  # [(220, 20, 60), ...]
    threshold: float = 0.5,
) -> np.ndarray:
    fig, ax = plt.subplots(figsize=(19, 11))
    ax.imshow(image)
    for label, score, (left, bot, right, top) in zip(labels, scores, bboxes):
        if score < threshold:
            continue
        x, y, w, h = left, bot, right - left, top - bot
        color = [c / 255 for c in palette[label]]
        rect = patches.Rectangle(
            (x, y), w, h, linewidth=1, edgecolor=color, facecolor="none"
        )
        ax.add_patch(rect)
        label_text = classes[label]
        ax.text(
            x,
            y,
            f"{label_text} {score * 100:.0f}%",
            bbox={"facecolor": "white", "alpha": 0.5},
        )

    count = count_boxes(labels, scores, classes, threshold)
    ax.set_title(f"{count}")
    ax.axis("off")
    fig.canvas.draw()
    width, height = fig.canvas.get_width_height()
    numpy_array = np.frombuffer(
        fig.canvas.tostring_argb(), dtype=np.uint8  # type: ignore
    ).reshape(height, width, 4)

    return numpy_array[:, :, 1:]


def draw_bboxes2(
    image: np.ndarray,
    bboxes: List[List[float]],
    labels: List[int],
    scores: List[float],
    classes: Tuple[str],  # ('Keenetic Start', ...)
    palette: List[Tuple[int]],  # [(220, 20, 60), ...]
    threshold: float = 0.5,
) -> np.ndarray:
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    for label, score, (left, bot, right, top) in zip(labels, scores, bboxes):
        if score < threshold:
            continue
        x, y, w, h = [int(val) for val in [left, bot, right - left, top - bot]]
        color = palette[label]
        cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
        label_text = classes[label]
        text = f"{label_text} {score * 100:.0f}%"
        draw_text(image, x, y, text)

    count = count_boxes(labels, scores, classes, threshold)
    draw_text(image, 10, 100, f"{count}", scale=1)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    return image


def draw_bboxes3(
    image: np.ndarray,
    bboxes: List[List[float]],
    labels: List[int],
    scores: List[float],
    classes: Tuple[str],  # ('Keenetic Start', ...)
    palette: List[Tuple[int, int, int]],  # [(220, 20, 60), ...]
    threshold: float = 0.5,
) -> np.ndarray:
    # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    palette = [color[::-1] for color in palette]  # RGB to BGR
    for label, score, (left, bot, right, top) in zip(labels, scores, bboxes):
        if score < threshold:
            continue
        x, y, w, h = [int(val) for val in [left, bot, right - left, top - bot]]
        color = palette[label]
        cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)

    text_height = int(
        1.3 * cv2.getTextSize("|", cv2.FONT_HERSHEY_SIMPLEX, 1, 1)[0][1]
    )
    boxes_count = count_boxes(labels, scores, classes, threshold)
    for i, (model, count) in enumerate(boxes_count.items()):
        text = f"{model}: {count}"
        background_color = (200, 200, 200) if model == "Total" else palette[i]
        draw_text(
            image,
            200,
            text_height * (i + 1),
            text,
            (0, 0, 0),
            background_color,
            scale=1,
        )
    # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    return image


def draw_text(
    image: np.ndarray,
    x: int,
    y: int,
    text: str,
    fg_color: Tuple[int, int, int] = (200, 200, 200),
    bg_color: Tuple[int, int, int] = (0, 0, 0),
    scale: float = 0.3,
    opacity: float = 0.5,
) -> np.ndarray:
    (w, h), _ = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, scale, 1)
    top = y - h
    bottom = y + 3
    left = x
    right = x + w
    sub_img = image[top:bottom, left:right]

    rect = np.full(sub_img.shape, bg_color, dtype=np.uint8)
    res = cv2.addWeighted(sub_img, opacity, rect, 1 - opacity, 1.0)
    res = cv2.putText(
        res,
        text,
        (0, h),
        cv2.FONT_HERSHEY_SIMPLEX,
        scale,
        fg_color,
        1,
        cv2.LINE_AA,
    )
    image[top:bottom, left:right] = res
    return image


def count_boxes(
    labels: List[int],
    scores: List[float],
    classes: Tuple[str],  # ('Keenetic Start', ...)
    threshold: float = 0.5,
) -> Dict[str, int]:
    boxes_count = Counter(dict.fromkeys(range(len(classes)), 0))
    for label, score in zip(labels, scores):
        boxes_count[label] += score >= threshold
    ret = {classes[label]: count for label, count in boxes_count.items()}
    ret["Total"] = sum(ret.values())
    return ret
