import glob
from pathlib import Path
import os
from typing import Any, Dict, Literal, List, Optional

import click
from dotenv import load_dotenv
import git
import mlflow
from mmdet.utils import setup_cache_size_limit_of_dynamo
from mmengine.config import Config
from mmengine.registry import VISBACKENDS
from mmengine.visualization import MLflowVisBackend
from mmengine.visualization.vis_backend import force_init_env
from mmengine.runner import Runner
import numpy as np
from PIL import Image
from torch import nn

from cv_boxes import WORK_DIR, TEMP_DIR
from cv_boxes.logging import get_logger
from cv_boxes.models.predict_model import _predict_image


@click.command()
@click.option(
    "-c",
    "--config",
    default=None,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-w",
    "--checkpoint",
    default=None,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-d",
    "--data-root",
    default=Path("data/processed/"),
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
)
@click.option(
    "--max-epochs",
    default=20,
    show_default=True,
    type=click.IntRange(0, 2**32 - 1),
)
@click.option(
    "-r",
    "--run-name",
    default=None,
    type=str,
    help="MLflow run name.",
)
@click.option(
    "-e",
    "--experiment-name",
    default="Default",
    type=str,
    help="MLflow experiment name.",
)
@click.option(
    "--threshold",
    default=0.5,
    show_default=True,
    type=click.FloatRange(0, 1),
)
@click.option(
    "--random-state",
    default=0,
    show_default=True,
    type=click.IntRange(0, 2**32 - 1),
)
@click.option(
    "-v", "--verbose", count=True, type=int, help="Increase verbosity level."
)
def train(
    config: Path,
    checkpoint: Optional[Path],
    data_root: Path,
    max_epochs: int,
    run_name: str,
    experiment_name: str,
    threshold: float,
    random_state: int,
    verbose: int,
) -> nn.Module:
    """
    \b
    Example:
    poetry run train \\
        -c checkpoints/mask-rcnn_r50-caffe_fpn_ms-poly-3x_router.py \\
        -w checkpoints/mask_rcnn_r50_caffe_fpn_mstrain-poly_3x_....pth \\
        -d data/processed/ \\
        --max-epochs 50 \\
        --run-name mask-rcnn_r50-caffe_fpn_ms-poly-3x_coco \\
        --threshold 0.792 \\
        -vvv
    """
    load_dotenv()
    setup_cache_size_limit_of_dynamo()

    logger = get_logger(verbose)
    logger.debug("Program started with verbosity level %s.", verbose)

    cfg = Config.fromfile(config)
    cfg.launcher = "none"
    model_name, _ = os.path.splitext(os.path.basename(config))
    cfg.work_dir = os.path.join(WORK_DIR, model_name)
    options = get_overriden_options_dict(
        model_name=model_name,
        checkpoint=checkpoint,
        data_root=data_root,
        max_epochs=max_epochs,
        run_name=run_name,
        experiment_name=experiment_name,
        random_state=random_state,
    )
    cfg.merge_from_dict(options)
    logger.debug("Config file %s loaded.", config)

    runner = Runner.from_cfg(cfg)
    run_id = get_run_id(runner)

    logger.debug("Starting train with run id %s.", run_id)
    trained_model = runner.train()
    trained_model.cfg = cfg
    trained_model.dataset_meta = cfg.metainfo
    logger.debug("Training for %d epochs fifnished.", max_epochs)

    # run inference on val images and log results and model
    with mlflow.start_run(run_id=run_id):
        repo = git.Repo(search_parent_directories=True)
        version = repo.head.object.hexsha
        mlflow.set_tag("mlflow.source.git.commit", version)

        logger.debug("Running inference on val dataset.")
        for image_path in get_images_list(cfg, data_root, "val", ["jpg"]):
            image = np.array(Image.open(image_path).convert("BGR;24"))
            image_name = os.path.basename(image_path)

            image = _predict_image(trained_model, image, threshold)
            image = image[:, :, ::-1]  # BGR to RGB
            mlflow.log_image(image, f"detected_routers/{image_name}")

        logger.debug("Saving model to MLflow Tracking.")
        mlflow.pytorch.log_model(runner.model, "mmdetection_model")

    return trained_model


def get_run_id(runner: Runner):
    backend = runner.visualizer.get_backend("MLflowVisBackendCustom")
    exp = backend.experiment
    run_id = exp.active_run().info.run_id
    return run_id


def get_images_list(
    config: Config,
    data_root: Path,
    subset: Literal["train", "val"],
    image_types: List[Literal["bmp", "jpg", "jpeg", "png"]],
) -> List[str]:
    """Returns list of images in val or train directory."""
    dataloader = subset + "_dataloader"
    images_dir = config.to_dict()[dataloader]["dataset"]["data_prefix"]["img"]
    directory = os.path.join(data_root, images_dir)
    ret = []
    for image_type in image_types:
        ret.extend(glob.glob(os.path.join(directory, f"*.{image_type}")))

    return ret


def get_overriden_options_dict(
    model_name: str,
    checkpoint: Optional[Path],
    data_root: Path,
    max_epochs: int,
    run_name: str,
    experiment_name: str,
    random_state: int,
) -> Dict[str, Any]:
    """Override some options in config according to command line arguments."""
    checkpoint_str = None if checkpoint is None else str(checkpoint)
    ret = {
        "load_from": checkpoint_str,
        "val_dataloader": {"dataset": {"data_root": str(data_root)}},
        "test_dataloader": {"dataset": {"data_root": str(data_root)}},
        "val_evaluator": {"ann_file": os.path.join(data_root, "val.json")},
        "test_evaluator": {"ann_file": os.path.join(data_root, "val.json")},
        "max_epochs": max_epochs,
        "train_cfg": {"max_epochs": max_epochs},
        "randomness": {"seed": random_state},
        "visualizer": {
            "name": "visualizer",
            "type": "DetLocalVisualizer",
            "vis_backends": [
                {"type": "LocalVisBackend"},
                {"type": "TensorboardVisBackend"},
                {
                    "type": "MLflowVisBackendCustom",
                    "save_dir": TEMP_DIR,
                    "exp_name": experiment_name,
                    "run_name": run_name,
                    "tags": None,
                    "params": None,
                    "tracking_uri": os.environ.get("MLFLOW_TRACKING_URI"),
                    "artifact_suffix": (".json", ".log", ".py", ".yaml"),
                },
            ],
        },
    }

    if model_name.startswith("ssd512"):
        ret["train_dataloader"] = {  # type: ignore
            "dataset": {"dataset": {"data_root": str(data_root)}}
        }
    else:
        ret["train_dataloader"] = {"dataset": {"data_root": str(data_root)}}

    return ret


@VISBACKENDS.register_module()
class MLflowVisBackendCustom(MLflowVisBackend):
    """Fix bug in MLflowVisBackend."""

    @force_init_env
    def add_config(self, config: Config, **kwargs) -> None:
        """Record the config to mlflow.

        Args:
            config (Config): The Config object
        """
        self.cfg = config
        if self._tracked_config_keys is None:
            # crop param length to mlflow limit
            cfg = self._flatten(self.cfg)
            for k, v in cfg.items():
                if len(str(v)) > 500:
                    cfg[k] = str(v)[:500]
            self._mlflow.log_params(cfg)
        else:
            tracked_cfg = {}
            for k in self._tracked_config_keys:
                tracked_cfg[k] = self.cfg[k]
            self._mlflow.log_params(self._flatten(tracked_cfg))
        self._mlflow.log_text(self.cfg.pretty_text, "config.py")
