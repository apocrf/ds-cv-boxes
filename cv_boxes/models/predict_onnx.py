# pylint:disable=duplicate-code
import os
from pathlib import Path
from typing import Dict, List, Tuple

import click
from mmdeploy_runtime import Detector  # pylint: disable = no-name-in-module
from mmdet.models.detectors.base import BaseDetector
import numpy as np
from PIL import Image

from cv_boxes.logging import get_logger
from cv_boxes.visualization.visualize import count_boxes, draw_bboxes3


CLASSES = (
    "Keenetic Air",
    "Keenetic Viva",
    "Keenetic Giga",
    "Keenetic Start",
)
PALETTE = [
    (220, 20, 60),
    (20, 220, 60),
    (20, 60, 220),
    (220, 220, 20),
]


@click.command()
@click.option(
    "-w",
    "--model-uri",
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
    default="models/chosen/",
    show_default=True,
)
@click.option(
    "-i",
    "--image-path",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-o",
    "--output-path",
    type=click.Path(path_type=Path),
)
@click.option(
    "--threshold",
    default=0.5,
    show_default=True,
    type=click.FloatRange(0, 1),
)
@click.option(
    "-v", "--verbose", count=True, type=int, help="Increase verbosity level."
)
def predict(
    model_uri: Path,
    image_path: Path,
    output_path: Path,
    threshold: float,
    verbose: int,
) -> None:
    """
    \b
    Example:
    poetry run predict_onnx \\
        -i data/processed/val/20231126113957280.jpg \\
        -o temp_dir/ \\
        --threshold 0.792 \\
        -vvv

    Copy URIs from MLflow Tracking server.
    """
    logger = get_logger(verbose)
    logger.debug("Program started with verbosity level %s.", verbose)

    model = Detector(model_path=str(model_uri), device_name="cpu", device_id=0)
    logger.debug("Model loaded from %s", model_uri)

    image = np.array(Image.open(image_path).convert("BGR;24"))
    logger.debug("Image loaded from %s", image_path)

    output_image = _predict_image(model, image, threshold)
    logger.debug("Predictions done.")

    image_name = os.path.basename(image_path)
    output_image = output_image[:, :, ::-1]  # BGR to RGB
    Image.fromarray(output_image).save(os.path.join(output_path, image_name))


def _predict_image(
    model: BaseDetector,
    image: np.ndarray,
    threshold: float,
) -> np.ndarray:
    scores, _, bboxes, labels, classes, palette = _predict(model, image)

    return draw_bboxes3(
        image=image,
        bboxes=bboxes,
        labels=labels,
        scores=scores,
        classes=classes,
        palette=palette,
        threshold=threshold,
    )


def _predict_text(
    model: BaseDetector,
    image: np.ndarray,
    threshold: float,
) -> Dict[str, int]:
    scores, _, _, labels, classes, _ = _predict(model, image)

    return count_boxes(
        labels=labels,
        scores=scores,
        classes=classes,
        threshold=threshold,
    )


def _predict(
    model: Detector, image: np.ndarray
) -> Tuple[List, List, List, List, Tuple, List[Tuple[int, int, int]]]:
    bboxes, labels, masks = model(image)
    scores = bboxes[:, -1].tolist()
    bboxes = bboxes[:, :4].astype(int).tolist()
    labels = labels.tolist()

    return scores, masks, bboxes, labels, CLASSES, PALETTE
