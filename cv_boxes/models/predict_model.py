import json
import os
from pathlib import Path
from typing import Dict, List, Tuple

import boto3
import click
from dotenv import load_dotenv
from mmdet.apis import inference_detector
from mmdet.models.detectors.base import BaseDetector
from mmdet.utils import setup_cache_size_limit_of_dynamo
from mmengine.config import Config
from mmengine.registry import init_default_scope
import mlflow
import numpy as np
from PIL import Image

from cv_boxes import WORK_DIR
from cv_boxes.data.auxiliary import load_annotations, get_images_list
from cv_boxes.logging import get_logger
from cv_boxes.visualization.visualize import count_boxes, draw_bboxes3


BUCKET_NAME = "mlflow"


@click.command()
@click.option("-c", "--config-uri", type=str)
@click.option("-w", "--model-uri", type=str)
@click.option(
    "-i",
    "--image-path",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-o",
    "--output-path",
    type=click.Path(path_type=Path),
)
@click.option(
    "--threshold",
    default=0.5,
    show_default=True,
    type=click.FloatRange(0, 1),
)
@click.option(
    "-v", "--verbose", count=True, type=int, help="Increase verbosity level."
)
def predict(
    config_uri: str,
    model_uri: str,
    image_path: Path,
    output_path: Path,
    threshold: float,
    verbose: int,
) -> None:
    """
    \b
    Example:
    poetry run predict \\
        -c s3://mlflow/0/<run-id>/artifacts/config.py \\
        -w runs:/<run-id>/mmdetection_model \\
        -i data/processed/val/20231103065632.jpg \\
        -o temp_dir/ \\
        --threshold 0.792 \\
        -vvv

    Copy URIs from MLflow Tracking server.
    """
    logger = get_logger(verbose)
    logger.debug("Program started with verbosity level %s.", verbose)

    model = load_model(config_uri, model_uri)
    logger.debug("Model loaded from %s", model_uri)

    image = np.array(Image.open(image_path).convert("BGR;24"))
    logger.debug("Image loaded from %s", image_path)

    output_image = _predict_image(model, image, threshold)
    logger.debug("Predictions done.")

    image_name = os.path.basename(image_path)
    output_image = output_image[:, :, ::-1]  # BGR to RGB
    Image.fromarray(output_image).save(os.path.join(output_path, image_name))


@click.command()
@click.option("-c", "--config-uri", type=str)
@click.option("-w", "--model-uri", type=str)
@click.option(
    "-i",
    "--images-dir",
    default=Path("data/raw/images/"),
    show_default=True,
    type=click.Path(exists=True, dir_okay=True, path_type=Path),
)
@click.option(
    "-a",
    "--annotations-path",
    default=Path("data/raw/annotations.json"),
    show_default=True,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-o",
    "--output-path",
    type=click.Path(path_type=Path),
)
@click.option(
    "--threshold",
    default=0.5,
    show_default=True,
    type=click.FloatRange(0, 1),
)
@click.option(
    "-v", "--verbose", count=True, type=int, help="Increase verbosity level."
)
def annotate_new_images(
    config_uri: str,
    model_uri: str,
    images_dir: Path,
    annotations_path: Path,
    output_path: Path,
    threshold: float,
    verbose: int,
) -> None:
    """
    \b
    Example:
    poetry run annotate_new_images \\
        -c s3://mlflow/0/<run-id>/artifacts/config.py \\
        -w runs:/<run-id>/mmdetection_model \\
        -i data/raw/images/ \\
        -a data/raw/annotations.json \\
        -o data/raw/new_annotations.json \\
        --threshold 0.792 \\
        -vvv

    Copy URIs from MLflow Tracking server.
    """
    logger = get_logger(verbose)
    logger.debug("Program started with verbosity level %s.", verbose)

    model = load_model(config_uri, model_uri)
    logger.debug("Model loaded from %s", model_uri)

    annotations = load_annotations(annotations_path)
    annotated_images = [image["file_name"] for image in annotations["images"]]
    image_id = max(image["id"] for image in annotations["images"]) + 1
    annotation_id = max(ann["id"] for ann in annotations["annotations"]) + 1
    images_list = get_images_list(images_dir, ["jpg"])
    unannotated_iamges = set(images_list) - set(annotated_images)

    for image_name in unannotated_iamges:
        image_path = os.path.join(images_dir, image_name)
        image = np.array(Image.open(image_path).convert("RGB"))
        logger.debug("Image loaded from %s", image_path)

        height, width, _ = image.shape
        annotations["images"].append(
            {
                "id": image_id,
                "width": width,
                "height": height,
                "file_name": image_name,
                "license": 0,
                "date_captured": "",
            }
        )

        scores, _, bboxes, labels, _, _ = _predict(model, image)
        for score, bbox, label in zip(scores, bboxes, labels):
            if score < threshold:
                continue
            left, top, right, bottom = bbox
            width = right - left
            height = bottom - top
            annotations["annotations"].append(
                {
                    "segmentation": [
                        [
                            left,
                            top,
                            right,
                            top,
                            right,
                            bottom,
                            left + 1,
                            bottom,
                        ]
                    ],
                    "area": width * height,
                    "bbox": [left, top, width, height],
                    "iscrowd": 0,
                    "id": annotation_id,
                    "image_id": image_id,
                    "category_id": label,
                }
            )
            annotation_id += 1

        image_id += 1

    with open(output_path, "w", encoding="utf-8") as f:
        json.dump(annotations, f)

    logger.debug("Predictions done.")


def _predict_image(
    model: BaseDetector,
    image: np.ndarray,
    threshold: float,
) -> np.ndarray:
    scores, _, bboxes, labels, classes, palette = _predict(model, image)

    return draw_bboxes3(
        image=image,
        bboxes=bboxes,
        labels=labels,
        scores=scores,
        classes=classes,
        palette=palette,
        threshold=threshold,
    )


def _predict_text(
    model: BaseDetector,
    image: np.ndarray,
    threshold: float,
) -> Dict[str, int]:
    scores, _, _, labels, classes, _ = _predict(model, image)

    return count_boxes(
        labels=labels,
        scores=scores,
        classes=classes,
        threshold=threshold,
    )


def _predict(
    model: BaseDetector, image: np.ndarray
) -> Tuple[List, List, List, List, Tuple[str], List[Tuple[int, int, int]]]:
    predictions = inference_detector(model, image)
    scores = predictions.pred_instances.scores.cpu().tolist()
    masks = predictions.pred_instances.masks.cpu().tolist()
    bboxes = predictions.pred_instances.bboxes.cpu().tolist()
    labels = predictions.pred_instances.labels.cpu().tolist()
    classes = model.dataset_meta["classes"]
    palette = model.dataset_meta["palette"]

    return scores, masks, bboxes, labels, classes, palette


def load_model(
    config_uri: str,
    model_uri: str,
) -> BaseDetector:
    """
    Load model from MLflow Server.

    Example:
    >>> model = load_model(
        "models:/Baseline/Production",
        "s3://mlflow/0/1f7a8b4f560d495e85a45610d754ed0b/artifacts/mas...py",
        logger
    )
    """
    load_dotenv()
    setup_cache_size_limit_of_dynamo()

    config = load_config(config_uri)
    init_default_scope(config.get("default_scope", "mmdet"))

    loaded_model = mlflow.pytorch.load_model(model_uri)
    loaded_model.cfg = config
    loaded_model.dataset_meta = config.metainfo

    return loaded_model


def load_config(config_uri: str) -> Config:
    """
    Load config from MLflow Server.

    Example:
    >>> config = load_config(
        "s3://mlflow/0/1f7a8b4f560d495e85a45610d754ed0b/artifacts/mas...py"
    )
    """
    minio_client = boto3.client(
        "s3", endpoint_url=os.environ.get("MLFLOW_S3_ENDPOINT_URL")
    )
    object_key = config_uri.replace(f"s3://{BUCKET_NAME}", "")

    response = minio_client.get_object(Bucket=BUCKET_NAME, Key=object_key)
    config = Config.fromstring(response["Body"].read().decode("utf-8"), ".py")
    config.work_dir = WORK_DIR
    config.launcher = "none"

    return config
