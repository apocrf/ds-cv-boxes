from io import BytesIO
import os

import click
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.responses import StreamingResponse
from mmdeploy_runtime import Detector  # pylint: disable = no-name-in-module
import numpy as np
from PIL import Image
import uvicorn

from cv_boxes.camera.rtsp import _capture_frame
from cv_boxes.models.predict_onnx import (
    _predict_image,
    _predict_text,
)
from cv_boxes.logging import get_logger


load_dotenv()
model = Detector(model_path="models/chosen/", device_name="cpu", device_id=0)
logger = get_logger(3)
app = FastAPI()

THRESHOLD = float(os.environ.get("THRESHOLD", 0.5))


@app.get("/predict-text/")
async def predict_text():
    return _predict_text(model, _capture_frame(), THRESHOLD)


@app.get("/predict-image/")
async def predict_image():
    output_image = _predict_image(model, _capture_frame(), THRESHOLD)

    return StreamingResponse(
        image_to_byte_stream(output_image), media_type="image/jpeg"
    )


def image_to_byte_stream(image: np.ndarray) -> BytesIO:
    ret = BytesIO()
    pil_image = Image.fromarray(image.astype("uint8")[:, :, ::-1])
    pil_image.save(ret, format="JPEG")
    ret.seek(0)

    return ret


@click.command()
def start():
    """
    Launched with `poetry run start` at root level.

    \b
    Test with:
    curl -X POST -H "Content-Type: multipart/form-data" \\
        -F "file=@data/raw/images/20230823190346.jpg" \\
        http://localhost:8000/predict-image/ --output out.jpg
    """
    uvicorn.run("cv_boxes.api.main:app", host="0.0.0.0", port=8000)
