import logging
import os

from cv_boxes import WORK_DIR


LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
LOG_PATH = f"{WORK_DIR}/cv-boxes.log"


def get_logger(verbose: int) -> logging.Logger:
    if verbose == 0:
        return FakeLogger()

    level = {
        1: logging.CRITICAL,
        2: logging.WARNING,
        3: logging.DEBUG,
    }[verbose]

    os.makedirs(WORK_DIR, mode=0o755, exist_ok=True)
    logging.basicConfig(
        format=LOG_FORMAT, filename=LOG_PATH, level=logging.WARNING
    )
    logger = logging.getLogger("cv-boxes")
    logger.setLevel(level)
    return logger


class FakeLogger(logging.Logger):
    def __init__(self, name="root", level=logging.NOTSET):
        super().__init__(name, level)

    def _log(
        self,
        level,
        msg,
        args,
        exc_info=None,
        extra=None,
        stack_info=False,
        **kwargs,
    ):
        pass

    def debug(self, msg, *args, **kwargs):
        pass

    def info(self, msg, *args, **kwargs):
        pass

    def warning(self, msg, *args, **kwargs):
        pass

    def error(self, msg, *args, **kwargs):
        pass

    def critical(self, msg, *args, **kwargs):
        pass
