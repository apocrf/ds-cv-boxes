import os
from pathlib import Path

ROOT_DIR = Path(__file__).parent.parent
print(ROOT_DIR)
WORK_DIR = os.path.join(ROOT_DIR, "work_dirs")
TEMP_DIR = os.path.join(ROOT_DIR, "temp_dir")
