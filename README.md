<a href="/LICENSE">
    <img alt="license" src="https://img.shields.io/gitlab/license/sergevv/ds-cv-boxes.svg?color=blue">
</a>
<a href="https://github.com/psf/black">
    <img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg">
</a>
<a href="https://gitlab.com/sergevv/ds-cv-boxes/-/commits/main">
    <img alt="pipeline status" src="https://gitlab.com/wervlad/ds-cv-boxes/badges/main/pipeline.svg" />
</a>

# Automated Stocktaking using computer vision (boxes with equipment)

Automatic inventory counting of available equipment in stock.

Currently the system confidently recognizes boxes with the following router models:\
✅ Keenetic Start\
✅ Keenetic Air\
✅ Keenetic Viva\
✅ Keenetic Giga

It can be further trained to account for customer-specific equipment.

# How it works

<div align="center">
    <img src="references/images/Infrastructure.jpg" alt="Infrastructure" width="80%"/>
</div>

User requests a report in the Telegram bot, which redirects it to FastAPI framework. Subsequently, an image request is made from the IP camera, and its inference is performed on the current production model from the MLflow registry. The report on the current availability of boxes with routers is then delivered back to Telegram bot.

MLflow, PostgreSQL, and MinIO S3 are deployed on a Raspberry Pi. Nginx handles authentication and traffic encryption. A VPN service is needed to tunnel access beyond a grey IP address. Each service operates in its own Docker container.

<details>
  <summary>Telegram bot</summary>
    <div align="center">
        <img src="references/images/TelegramBot.png" alt="Telegram Bot" width="50%"/>
    </div>
</details>
<details>
  <summary>Report examples</summary>
    <div align="center">
        <img src="references/images/Report1.jpg" alt="Report Example 1" width="40%"/>
        <img src="references/images/Report2.jpg" alt="Report Example 2"  width="40%"/>
    </div>
</details>
<details>
  <summary>MLflow Tracking</summary>
    <div align="center">
        <img src="references/images/MLflow.png" alt="MLflow" width="80%"/>
    </div>
</details>


# Installation
First clone this repository.

If you only need API:
```bash
poetry install
```
For running bot you'll need only bot dependencies:
```bash
poetry install --only main --only bot
```
For development purposes, execute the following command.:
```bash
poetry install --with dev --with bot --with test --with jupyter --with nvidia
```
Skip `--with nvidia` option if you're only planning to run inference on CPU.


# Configuration
In the project's root directory, create a file named `.env` and fill it like this:
```ini
MLFLOW_TRACKING_URI=<Tracking Server URI>
MLFLOW_S3_ENDPOINT_URL=<MinIO S3 URI>
MLFLOW_TRACKING_USERNAME=<Your Login to Tracking Server>
MLFLOW_TRACKING_PASSWORD=<Your password to Tracking Server>

MODEL_URI=models:/Baseline/Production
CONFIG_URI=<Model Config URI>
THRESHOLD=<Model Confidence Threshold>

TELEGRAM_TOKEN=<Telegram Bot Token>
```
Please replace values in <...> with your credentials and desired values. `TELEGRAM_TOKEN` is only needed for launching the bot.

Add your MinIO credentials to ~/.aws/credentials
```ini
[default]
aws_access_key_id = <MinIO access key>
aws_secret_access_key = <MinIO secret key>
```

# Usage
### Start API
```bash
poetry run start_api
```
or
```bash
make start-api
```
### Start telegram bot
```bash
poetry run start_bot
```
or
```bash
make start-bot
```


# Development

## Dataset management

To utilize data version control, please install [dvc](https://dvc.org/).

Download files:
```bash
dvc pull
```
Images and annotations should appear in the data/raw directory. Adding new/modified files works similarly to Git. To view changes since the last commit:
```bash
dvc diff
```
Add the changes:
```bash
dvc add data/raw/<image-name.jpg>
```
This updates the metadata. Then, add the changes to Git:
```bash
git diff
git add data.dvc
git commit -m "Add images to the dataset"
```
If the commit message contains keywords `dvc` and `dataset` the pipeline for this commit won't run to save compute minutes.

### Split dataset into train & test subsets
```bash
poetry run train_test_split --help      # show help for command options

poetry run train_test_split \
    -i data/raw/ \                      # path to original images
    -o data/processed/ \                # path to produced datasets
    --annotations annotations.json \    # annotations filename
    --train-size 0.8 \                  # train size ratio
    --random-state 0                    # random seed
```


## Train model
```bash
poetry run train --help                                                 # show help for command options

poetry run train \
    -c checkpoints/mask-rcnn_r50-caffe_fpn_ms-poly-3x_router.py \       # path to model config
    -w checkpoints/mask_rcnn_r50_caffe_fpn_mstrain-poly_3x_....pth \    # path to pretrained model weights
    -d data/processed/ \                                                # path to dataset
    --max-epochs 50 \                                                   # number of epochs to train for
    --run-name mask-rcnn_r50-caffe_fpn_ms-poly-3x_coco \                # run name
    --threshold 0.792 \                                                 # confidence threshold for validation
    -vvv
```

## Make predictions

```bash
poetry run train --help                             # show help for command options

poetry run predict \
    -c s3://mlflow/0/<run-id>/artifacts/config.py \ # config URI
    -w runs:/<run-id>/mmdetection_model \           # model URI
    -i data/processed/val/20231103065632.jpg \      # path to input image
    -o temp_dir/ \                                  # output dir
    --threshold 0.792 \                             # confidence threshold
    -vvv                                            # verbosity level
```


# Run tests and linters
```bash
make tests
```

<details>
  <summary>Project files description</summary>
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── pyproject.toml     <- The requirements file for reproducing the analysis environment
    │
    ├── poetry.lock        <- Hash dependency
    │
    ├── cv_boxes           <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── start.py           <- Start script


--------
</details>

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
